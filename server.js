﻿/* Initialise http server using Connect as requestListener. */
var connect = require('connect');
var requestListener = connect();
var httpServer = require('http').createServer(requestListener);

/* Initialise life engine */
var lifeEngine = require('./lifeEngine')({ gridColumns: 40, gridRows: 40, tickInterval: 100 });

/* Tell Connect to serve static files out of public folder */
requestListener.use('/', connect.static('public', { index: 'nodelife.html' }));

/* Start http server listening. process.env.PORT is assigned by iisnode */
httpServer.listen(process.env.PORT); 
console.log('Listening on port: ' + process.env.PORT);

/* Start socket.io listening on same port as http server */
require('socket.io').listen(httpServer).sockets.on('connection', function (socket) {

    var connected = true;

    socket.on('disconnect', function () { connected = false; });
    socket.on('flipCell', function (data) { lifeEngine.flipCell(data); });
    socket.on('start', function () { lifeEngine.start(); });
    socket.on('stop', function () { lifeEngine.stop(); });
    socket.on('step', function () { lifeEngine.step(); });
    socket.on('clear', function () { lifeEngine.clear(); });
    socket.on('loadPattern', function (name) { lifeEngine.clear(name); });

    // register this new socket connection for state changes from the life engine
    lifeEngine.onStateChange(function(cellStates) { if (connected) socket.emit('cellStates', cellStates); });

    // send the new client an init message to setup its grid
    socket.emit('init', lifeEngine.gridDimension());
});

