﻿var _ = require('underscore');

/* Cell constructor */
var Cell = function (x, y) {

    this.x = x;
    this.y = y;
    this.neighbours = [];
    this.state = false;
    this.nextState = false;
};

/* Conway Life algorithm implementation - determines when a cell should change state. */
Cell.prototype.calcNextState = function (stateChange) {

    this.nextState = this.state;
    var liveNeighbours = _.where(this.neighbours, { state: true }).length;

    if (this.state && (liveNeighbours < 2 || liveNeighbours > 3))
        this.nextState = false;

    if (!this.state && liveNeighbours == 3)
        this.nextState = true;
};

var lifeEngine = function (options) {

    options = options || {};
    var tickInterval = options.tickInterval || 10;
    var gridColumns = options.gridColumns || 80;
    var gridRows = options.gridRows || 60;
    var grid = [];
    var stateBuffer = [];
    var clients = [];
    var running;

    /* Setup grid of cells */
    for (var y = 0; y < gridRows; y++)
        for (var x = 0; x < gridColumns; x++)
            grid.push(new Cell(x, y));

    /* Given the array just created, this function gets a Cell from its x/y coordinates  */
    grid.cellAt = function(coord) { return this[coord.x + coord.y * gridColumns]; };

    /* Connect each cell with its 8 immediate neighbours */
    _.each(grid, function (cell) {

        for (var y = Math.max(0, cell.y - 1) ; y < Math.min(gridRows, cell.y + 2) ; y++)
            for (var x = Math.max(0, cell.x - 1) ; x < Math.min(gridColumns, cell.x + 2) ; x++)
                if (!(x == cell.x && y == cell.y))
                    cell.neighbours.push(grid.cellAt({ x: x, y: y }));
    });

    /* Take a set of cell coordinates and centre the whole pattern within the current defined grid bounds */
    var centralise = function(pattern) {

        var left = _.min(_.pluck(pattern, 'x'));
        var right = _.max(_.pluck(pattern, 'x'));
        var top = _.min(_.pluck(pattern, 'y'));
        var bottom = _.max(_.pluck(pattern, 'y'));

        var xOffset = Math.floor((gridColumns - (right - left + 1)) / 2) - left;
        var yOffset = Math.floor((gridRows - (bottom - top + 1)) / 2) - top;

        return _.map(pattern, function(cell) { return { x: cell.x + xOffset, y: cell.y + yOffset } });
    };

    /* push cell states onto buffer for update to clients */
    stateBuffer.pushCellStates = function(cells) { this.push(_.map(cells, function (cell) { return { x: cell.x, y: cell.y, on: cell.state }; })); };

    /* Push a message onto the buffer for all cells that are changing state */
    var pushStateChange = function () {

        var stateChange = [];
        _.each(_.filter(grid, function (cell) { return cell.nextState != cell.state; }), function (cell) {

            cell.state = cell.nextState;
            stateChange.push(cell);
        });

        if (stateChange.length > 0)
            stateBuffer.pushCellStates(stateChange);
    };

    /* State transition loop */
    var tick = function () {

        _.each(grid, function (cell) { cell.calcNextState(); });
        pushStateChange();

        if (running) setTimeout(tick, tickInterval);
    };

    /* Invoke state change handler function for each registered client */
    var updateClients = function () {

        if (stateBuffer.length > 0) {

            var cellStates = stateBuffer.shift();
            _.each(clients, function(stateChangeHandler) { stateChangeHandler(cellStates); });
        }
    };

    /* Start client update loop */
    setInterval(updateClients, tickInterval);

    console.log('lifeEngine initialised');

    /* Expose public methods of lifeEngine */
    return {

        gridDimension: function() { return { gridColumns: gridColumns, gridRows: gridRows }; },

        /* Call this to register a handler function that will receive state changes */
        onStateChange: function(handler) {
         
            // suspend state transitions 
            var wasRunning = running;
            running = false;

            // after allowing time for next state transition to complete, push current grid state onto client update buffer
            // this ensures new clients are brought up to speed with current state of the grid; after that, they only receive deltas
            setTimeout(function () {

                stateBuffer.pushCellStates(_.where(grid, { state: true }));

                // enlist new client for ongoing state update events
                clients.push(handler);

                // re-start state transitions if they were running
                if (wasRunning) {

                    running = true;
                    tick();
                }

            }, 100);            
        },

        // Flip a cell and push the state change back to clients to update their displays
        flipCell: function (coord) {

            var cell = grid.cellAt(coord); //grid[vectorToScalar(v)];
            cell.state = !cell.state;
            stateBuffer.pushCellStates([cell]);
        },

        start: function () { running = true; tick(); },
        stop: function () { running = false; },
        step: function () { if (!running) tick(); },

        /* Stop running and clear the grid; after delay to avoid any race condition with tick() */
        clear: function (patternName) {

            running = false;

            setTimeout(function () {

                _.each(grid, function (cell) { cell.nextState = false; });

                if (patternName) {
                    
                    var pattern = require('./patterns/' + patternName + '.json');
                    _.each(centralise(pattern), function (coord) { grid.cellAt(coord).nextState = true; });
                }

                pushStateChange();

            }, 100);
        }
    };
};

module.exports = lifeEngine;